/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

public class Calculo {

    int capital;
    int interes;
    int numero;
   
    public float getResultado(int c, int i, int n){
        float resp;
        float interes = (float)i/100; 
     
        resp = (c * interes ) * n;
        
        return resp;
    }
    
    public int getCapital() {
        return capital;
    }

    public int getInteres() {
        return interes;
    }

    public int getNumero() {
        return numero;
    }

    public void setCapital(int capital) {
        this.capital = capital;
    }

    public void setInteres(int interes) {
        this.interes = interes;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }
    

}
