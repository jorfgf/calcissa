package Controller;

import Modelo.Calculo;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "controller", urlPatterns = {"/controller"})
public class calculoController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet controller</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet controller at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        
      

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        
         String capital = (String) request.getParameter("capital");
            String interes = (String) request.getParameter("interes");
            String numero = (String) request.getParameter("numero");
            
            
            int c = Integer.parseInt(capital);
            int i = Integer.parseInt(interes);
            int n = Integer.parseInt(numero);
            
             Calculo interesSimple = new Calculo();
          
            float resp = interesSimple.getResultado(c,i,n);
           
           
            request.setAttribute("resultado", Float.toString(resp));
            request.getRequestDispatcher("vistas/resultado.jsp").forward(request, response);
            //RequestDispatcher rd = request.getRequestDispatcher("resultado.jsp");
            //rd.forward(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
